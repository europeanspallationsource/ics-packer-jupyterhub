#!/bin/bash

set -e

# Install docker on the jupyterhub container
wget https://get.docker.com -q -O /tmp/getdocker
chmod +x /tmp/getdocker
sh /tmp/getdocker
rm -f /tmp/getdocker

# Install dockerspawner and ldapauthenticator
# Use a specific commit for ldapauthenticator because the following
# pull request is needed but has not been merged yet:
# https://github.com/jupyterhub/ldapauthenticator/pull/32
/opt/conda/bin/pip install \
    dockerspawner==0.7.0 \
    git+https://github.com/mateuszboryn/ldapauthenticator.git@ccdd2aa26cf2751bf88f954316fbe5292808ecb2
# To be replaced with official version when pull request merged
#    jupyterhub-ldapauthenticator==x.x
