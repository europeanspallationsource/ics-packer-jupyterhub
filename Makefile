.PHONY: help build

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build       to build the docker image"

build:
	packer build jupyterhub.json
