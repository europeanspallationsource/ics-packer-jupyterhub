Packer template to build jupyterhub ESS Docker image
====================================================

This Packer_ template creates a Docker_ image based on `jupyterhub/jupyterhub`.

It adds docker engine dockerspawner and ldapauthenticator.


Usage
-----

To be able to push the image to Docker Hub, you first have to login::

    $ docker login

You can then run::

    $ packer build jupyterhub.json

    or

    $ make build

This will create and push the image `europeanspallationsource/jupyterhub:latest`.

Note that you can pass a specific tag as variable::

    $ packer build -var "jupyterhub_tag=1.0.0" jupyterhub.json


.. _Packer: https://www.packer.io
.. _Docker: https://www.docker.com
